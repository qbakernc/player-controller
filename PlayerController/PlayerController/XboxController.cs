﻿using System;
using System.Threading;
using SharpDX.XInput;

namespace PlayerController
{
    class XboxController
    {
		/// <summary>
		/// ButtonPressEventHandler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		public delegate void ButtonPressEventHandler(object sender, ControllerEventArgs args);

		/// <summary>
		/// Event will be triggered once a button on the controller has been pressed.
		/// </summary>
		public event ButtonPressEventHandler OnButtonPress;

        private Controller controller;
        private Timer timer;

		// The previous state of the button. True or false.
		private bool startPreviousState;
        private bool aPreviousState;
        private bool upPreviousState;
        private bool downPreviousState;
        private bool leftPreviousState;
        private bool rightPreviousState;
        public XboxController()
        {
			object asdf = UserIndex.One;
            controller = new Controller(UserIndex.One);
        }
		/// <summary>
		/// Starts the poll for button state changes on the controller.
		/// </summary>
		public void ReadButtonPresses()
        {
			timer = new Timer(Update, null, 0, 17);

			while(Console.ReadKey(true).Key != ConsoleKey.Escape) {
				/// will exit the program when the escape key has been pressed.
            }
		}
		/// <summary>
		/// A constant check for a button state change on the controller. Will be polled 60 times per second.
		/// </summary>
		/// <param name="input"></param>
        private void Update(object input)
        {
			controller.GetState(out State state);
			StartButton(state);
			AButton(state);
			UpButton(state);
			DownButton(state);
			RightButton(state);
			LeftButton(state);
		}
		/// <summary>
		/// Has the start button been pressed.
		/// </summary>
		/// <param name="state"></param>
		private void StartButton(State state)
		{
			bool isPressed = state.Gamepad.Buttons.HasFlag(GamepadButtonFlags.Start);

			if (isPressed && !startPreviousState) 
				OnButtonPress?.Invoke(this, new ControllerEventArgs("q"));

			startPreviousState = isPressed;
		}
		/// <summary>
		/// Has the A button been pressed.
		/// </summary>
		/// <param name="state"></param>
		private void AButton(State state)
		{
			bool isPressed = state.Gamepad.Buttons.HasFlag(GamepadButtonFlags.A);

			if (isPressed && !aPreviousState)
				OnButtonPress?.Invoke(this, new ControllerEventArgs(" "));

			aPreviousState = isPressed;
		}
		/// <summary>
		/// Has Up on the D pad been pressed.
		/// </summary>
		/// <param name="state"></param>
		private void UpButton(State state)
		{
			bool isPressed = state.Gamepad.Buttons.HasFlag(GamepadButtonFlags.DPadUp);

			if (isPressed && !upPreviousState)
				OnButtonPress?.Invoke(this, new ControllerEventArgs("w"));

			upPreviousState = isPressed;
		}
		/// <summary>
		/// Has Down on the D ad been pressed.
		/// </summary>
		/// <param name="state"></param>
		private void DownButton(State state)
		{
			bool isPressed = state.Gamepad.Buttons.HasFlag(GamepadButtonFlags.DPadDown);

			if (isPressed && !downPreviousState)
				OnButtonPress?.Invoke(this, new ControllerEventArgs("s"));
			
			downPreviousState = isPressed;
		}
		/// <summary>
		/// Has Right on the D pad been pressed.
		/// </summary>
		/// <param name="state"></param>
		private void RightButton(State state)
		{
			bool isPressed = state.Gamepad.Buttons.HasFlag(GamepadButtonFlags.DPadRight);

			if (isPressed && !rightPreviousState)
				OnButtonPress?.Invoke(this, new ControllerEventArgs("d"));

			rightPreviousState = isPressed;
		}
		/// <summary>
		/// Has Left on the D pad been pressed.
		/// </summary>
		/// <param name="state"></param>
		private void LeftButton(State state)
		{
			bool isPressed = state.Gamepad.Buttons.HasFlag(GamepadButtonFlags.DPadLeft);

			if (isPressed && !leftPreviousState)
				OnButtonPress?.Invoke(this, new ControllerEventArgs("a"));

			leftPreviousState = isPressed;
		}
	}
}
