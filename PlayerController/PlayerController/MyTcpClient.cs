﻿using System;
using System.IO;
using System.Text;
using System.Net.Sockets;
using System.Threading;

namespace PlayerController
{
    class MyTcpClient
    {
        TcpClient tcpClient;                                 // Client that will connect to the crestron processor.
        ConsoleKeyInfo consoleKey;                           // Received keystroke.
        private const string acceptedValues = "1234567890";  // Accepted values for the port number.
        private string hostname;                             // Hostname of the processor.
        private string port;                                 // port number of the server we are connecting to.

        /// <summary>
        /// Configures the tcpClient.
        /// </summary>
        public void Configure()
        {
            Console.WriteLine("Please enter the Processor's hostname.");
            hostname = Console.ReadLine();

            Console.WriteLine("Please enter the Port number.");
            do
            {
                consoleKey = Console.ReadKey(true);
                if (consoleKey.Key == ConsoleKey.Backspace && port.Length > 0)
                {
                    port = port.Remove(port.Length - 1, 1);
                    Console.Write("\b \b");
                }
                else if (acceptedValues.Contains(consoleKey.KeyChar))
                {
                    port += consoleKey.KeyChar;
                    Console.Write(consoleKey.KeyChar);
                }
            } while (consoleKey.Key != ConsoleKey.Enter);
          
            try
            {
                Console.WriteLine("");
                tcpClient = new TcpClient(hostname, int.Parse(port));
                ReadTask();
            }
            catch (Exception)
            {
                Console.WriteLine("Connection Failed. Please verify that the hostname and port number are correct. ");
                Configure();
            }
            Thread t = new Thread(ReadTask);
            t.Start();
        }
        /// <summary>
        /// Sends data to the server
        /// </summary>
        /// <param name="data">Data that will be sent. </param>
        public async void ReadTask()
        {
            try
            {
                byte[] data = new byte[100];
                NetworkStream stream = tcpClient.GetStream();
                await stream.ReadAsync(data, 0, 100);
                Console.WriteLine(Encoding.UTF8.GetString(data));
                Thread t = new Thread(ReadTask);
                t.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        /// <summary>
        /// Sends data to the server
        /// </summary>
        /// <param name="data">Data that will be sent. </param>
        public void SendData(string data)
        {
            try
            {
                NetworkStream stream = tcpClient.GetStream();
                stream.Write(Encoding.UTF8.GetBytes(data));

            } catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
