﻿using System;
using System.Threading;

namespace PlayerController
{
    class Program
    {
        private static string usingController;
        private static MyTcpClient myTcpClient;
        static void Main(string[] args)
        {
            myTcpClient = new MyTcpClient();
            myTcpClient.Configure();
            while (true) {
                Console.WriteLine("Using a controller? y/n");
                usingController = Console.ReadLine().ToLower();
                if (usingController == "y") {
                    XboxController xboxController = new XboxController();
                    xboxController.OnButtonPress += XboxController_OnButtonPress;
                    Thread t = new Thread(xboxController.ReadButtonPresses);
                    t.Start();
                    break;
                } else if (usingController == "n") {
                    KeyboardController keyboardController = new KeyboardController();
                    keyboardController.OnKeyPress += KeyboardController_OnKeyPress;
                    Thread t = new Thread(keyboardController.ReadKeyPresses);
                    t.Start();
                    break;
                }
            }
           
        }

        private static void KeyboardController_OnKeyPress(object sender, ControllerEventArgs args)
        {
            myTcpClient.SendData(args.KeyPressed);
        }

        private static void XboxController_OnButtonPress(object sender, ControllerEventArgs args)
        {
            myTcpClient.SendData(args.KeyPressed);
        }
    }
}
