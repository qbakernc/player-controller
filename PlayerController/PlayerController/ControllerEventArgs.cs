﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlayerController
{
    class ControllerEventArgs : EventArgs
    {
        private string keyPressed;
        public string KeyPressed
        {
            get {
                return keyPressed;
            }
        }
        public ControllerEventArgs(string keyPressed)
        {
            this.keyPressed = keyPressed;
        }
    }
}
