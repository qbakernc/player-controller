﻿using System;

namespace PlayerController
{
    class KeyboardController
    {
        public delegate void KeyPressEventHandler(object sender, ControllerEventArgs args);
        public event KeyPressEventHandler OnKeyPress;

        ConsoleKey consoleKey;

        /// <summary>
        /// Reads keyboard presses for controls.
        /// </summary>
        public void ReadKeyPresses()
        {
            do
            {
                consoleKey = Console.ReadKey(true).Key;

                switch (consoleKey) {
                    case ConsoleKey.Spacebar:
                        OnKeyPress?.Invoke(this, new ControllerEventArgs(" "));
                        break;
                    case ConsoleKey.Enter:
                        OnKeyPress?.Invoke(this, new ControllerEventArgs("q"));
                        break;
                    case ConsoleKey.W:
                        OnKeyPress?.Invoke(this, new ControllerEventArgs("w"));
                        break;
                    case ConsoleKey.A:
                        OnKeyPress?.Invoke(this, new ControllerEventArgs("a"));
                        break;
                    case ConsoleKey.S:
                        OnKeyPress?.Invoke(this, new ControllerEventArgs("s"));
                        break;
                    case ConsoleKey.D:
                        OnKeyPress?.Invoke(this, new ControllerEventArgs("d"));
                        break;
                    case ConsoleKey.UpArrow:
                        OnKeyPress?.Invoke(this, new ControllerEventArgs("w"));
                        break;
                    case ConsoleKey.LeftArrow:
                        OnKeyPress?.Invoke(this, new ControllerEventArgs("a"));
                        break;
                    case ConsoleKey.DownArrow:
                        OnKeyPress?.Invoke(this, new ControllerEventArgs("s"));
                        break;
                    case ConsoleKey.RightArrow:
                        OnKeyPress?.Invoke(this, new ControllerEventArgs("d"));
                        break;
                } 
            } while (consoleKey != ConsoleKey.Escape) ;
        }
    }
}
